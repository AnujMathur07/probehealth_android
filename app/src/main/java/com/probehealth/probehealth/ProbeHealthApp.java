package com.probehealth.probehealth;

import android.app.Application;

public class ProbeHealthApp extends Application
{
    @Override
    public void onCreate()
    {
        super.onCreate();
        // initializations to be used globally.
    }
}
