package com.probehealth.probehealth.fragments;

import android.support.v4.app.Fragment;

abstract public class BaseFragment extends Fragment
{
    /**
     * Change method arguments according to use
     * @return Instance of this fragment
     */
    abstract Fragment newInstance();
}
