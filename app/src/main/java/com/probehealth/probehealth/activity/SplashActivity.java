package com.probehealth.probehealth.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.probehealth.probehealth.R;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Initialize Crashlytics. Do not this line.
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_splash);
    }
}
