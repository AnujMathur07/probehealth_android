package com.probehealth.probehealth.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Base Activity for all all other activities
 */
public class BaseActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // initializations and task you want to perform in every Activity.
    }
}
